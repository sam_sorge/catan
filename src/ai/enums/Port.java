package ai.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Port {
	GENERAL(2, Arrays.asList(Resource.BRICK, Resource.ROCK, Resource.SHEEP, Resource.WHEAT, Resource.WOOD)),
	SHEEP(3, Arrays.asList(Resource.SHEEP)),
	WHEAT(3, Arrays.asList(Resource.WHEAT)),
	ROCK(3, Arrays.asList(Resource.ROCK)),
	WOOD(3, Arrays.asList(Resource.WOOD)),
	BRICK(3, Arrays.asList(Resource.BRICK));
		
	private final Integer multiplier;
	private final List<Resource> resources = new ArrayList<>();

    private Port(Integer multiplier, List<Resource> resources) {
        this.multiplier = multiplier;
    }

    public Integer getMultiplier() {
        return multiplier;
    }
    
    public List<Resource> getResources() {
    	return resources;
    }
}
