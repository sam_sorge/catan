package ai.enums;

public enum Resource {
	DESERT,
	WOOD,
	BRICK,
	SHEEP,
	ROCK,
	WHEAT
}
