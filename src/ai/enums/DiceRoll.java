package ai.enums;

public enum DiceRoll {
	
	TWO(1),
	THREE(2),
	FOUR(3),
	FIVE(4),
	SIX(5),
	SEVEN(6),
	EIGHT(5),
	NINE(4),
	TEN(3),
	ELEVEN(2),
	TWELVE(1);

	private final Integer odds;

	    private DiceRoll(Integer odds) {
	        this.odds = odds;
	    }

	    public Integer getOdds() {
	        return odds;
	    }
	
}
