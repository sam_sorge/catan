package ai;

public class Coordinate {

	private int coord1;
	
	private int coord2;
	
	public Coordinate(int coord1, int coord2) {
		super();
		this.coord1 = coord1;
		this.coord2 = coord2;
	}

	public int getCoord1() {
		return coord1;
	}

	public void setCoord1(int coord1) {
		this.coord1 = coord1;
	}

	public int getCoord2() {
		return coord2;
	}

	public void setCoord2(int coord2) {
		this.coord2 = coord2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + coord1;
		result = prime * result + coord2;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinate other = (Coordinate) obj;
		if (coord1 != other.coord1)
			return false;
		if (coord2 != other.coord2)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "(" + coord1 + ", " + coord2 + ")";
	} 
	
}
