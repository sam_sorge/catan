package ai;

import java.util.ArrayList;
import java.util.List;

import ai.enums.DiceRoll;
import ai.enums.Resource;

public class Hex {
	
	Coordinate coordinate;
	List<Edge> edges = new ArrayList<>();
	List<Point> points = new ArrayList<>();
	Resource resource;
	DiceRoll diceRoll;
	
	Hex(Coordinate coord) {
		this.coordinate = coord;
	}
	
	Hex(int x, int y) {
		this.coordinate = new Coordinate(x, y);
	}
	
	Hex(int x, int y, Resource resource, DiceRoll diceRoll) {
		this.coordinate = new Coordinate(x, y);
		this.resource = resource;
		this.diceRoll = diceRoll;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public DiceRoll getDiceRoll() {
		return diceRoll;
	}

	public void setDiceRoll(DiceRoll diceRoll) {
		this.diceRoll = diceRoll;
	}
	
	public Integer getHeuristic(Player player) {
		
		
		
		
		
		return 0;
	}
	
		
	
	
}
