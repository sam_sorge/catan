package ai;

import java.util.Map;
import java.util.Map.Entry;

import ai.enums.Resource;

public class Move implements Comparable<Move> {
	Integer playerNumber;
	
	Coordinate coordinate;
	
	Board board; // ???
	
	public Move(Integer playerNumber, Coordinate coordinate, Board board) {
		this.playerNumber = playerNumber;
		this.coordinate = coordinate;
		this.board = board;
	}

	public Integer getPlayerNumber() {
		return playerNumber;
	}

	public void setPlayerNumber(Integer playerNumber) {
		this.playerNumber = playerNumber;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public int calculateHeuristic() {
		
		int heuristic = 0;
		
		Map<Resource, Integer> playerResources = board.getPlayerResources(playerNumber);
		
		Point point = board.getPoint(coordinate);
		
		for (Hex hex: board.getHexesForPoint(coordinate)) {
			if (hex != null) {
				playerResources.put(hex.getResource(), playerResources.get(hex.getResource()));
			}
		}
		
		for (Edge edge: board.getEdgesForPoint(coordinate)) {
			if (edge != null && edge.getPort() != null) {
				for (Resource portResource : edge.getPort().getResources()) {
					playerResources.put(portResource, playerResources.get(portResource) * edge.getPort().getMultiplier());
				}
			}
		}
		
		for (Entry<Resource, Integer> resource: playerResources.entrySet()) {
			if (resource.getValue() > 0) {
				heuristic += 25;
			}
		}
		
		
		
		
		return heuristic;
	}

	@Override
	public int compareTo(Move arg) {
		if (arg.calculateHeuristic() > this.calculateHeuristic()) {
			return 1;
		} else if (arg.calculateHeuristic() < this.calculateHeuristic()) {
			return -1;
		} else {
			return 0;
		}
	}
	
	
		
}
