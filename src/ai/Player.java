package ai;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;

import ai.enums.Resource;

public class Player {
	private int playerId;
	private List<Point> points = new ArrayList<>();

	public Player(int playerId) {
		this.playerId = playerId;
	}

	// TODO

	public Coordinate chooseInitialPlacement(Board board) {
		PriorityQueue<Move> queue = new PriorityQueue<Move>();

		Map<Resource, Integer> myResources = board.getPlayerResources(playerId);

		for (Entry<Coordinate, Point> point : board.getPoints().entrySet()) {
			Coordinate key = point.getKey();
			Point value = point.getValue();

			if (board.isSettleable(key)) {
				queue.add(new Move(playerId, key, board));
			}

			System.out.println(value.getCoordinate().toString() + ": ");

			for (Hex hex : value.getHexes()) {
				System.out.println("\t" + hex.getCoordinate().toString());
			}

		}
		
		return null;
	}
}
