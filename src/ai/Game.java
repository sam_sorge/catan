package ai;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Map.Entry;

public class Game {
	Map<Integer, Player> players = new HashMap<>();
	
	Board board;
	
	Game(int numPlayers) {
		board = new Board();
		for (Entry<Coordinate, Point> point : board.getPoints().entrySet()) {
		    Coordinate key = point.getKey();
		    Point value = point.getValue();
		    
		    System.out.println(value.getCoordinate().toString() + ": ");
		    
		    for (Hex hex : value.getHexes()) {
			   System.out.println("\t" + hex.getCoordinate().toString());
		    }
 
		}
		
		for (int i = 0; i < numPlayers; i++) {
			players.put(i, new Player(i));
		}
		
	}

	public Map<Integer, Player> getPlayers() {
		return players;
	}

	public void setPlayers(Map<Integer, Player> players) {
		this.players = players;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}
	
	public Player getPlayer(int i) {
		return players.get(i);
	}
	
	public void makeInitialPlacements() {
		for (int i = 0; i < players.size(); i++) {
			PriorityQueue<Move> queue = new PriorityQueue<Move>();

			List<Coordinate> points = board.getSettleablePoints();
			int max = 0;
			
			for (Entry<Coordinate, Point> point : board.getPoints().entrySet()) {
			    Coordinate key = point.getKey();
			    Point value = point.getValue();
			    
			    if (board.isSettleable(key)) {
			    	queue.add(new Move(i, key, board));
			    }
			    
			    System.out.println(value.getCoordinate().toString() + ": ");
			    
			    for (Hex hex : value.getHexes()) {
				   System.out.println("\t" + hex.getCoordinate().toString());
			    }
	 
			}
			
//			getPlayer(i).makeInitialPlacement(board);
			// choose placement
		}
		
		for (int i = players.size() - 1; i >= 0; i--) {
			// choose placement
		}
	}
}
