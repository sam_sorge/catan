package ai;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ai.enums.DiceRoll;
import ai.enums.Resource;

public class Board {
	
	Map<Coordinate, Hex> hexes = new HashMap<>();
	Map<Coordinate, Point> points = new HashMap<>();
	Map<Coordinate, Edge> edges = new HashMap<>();

	Board() {
		createPorts();
		
		addHex(1, 5, Resource.WOOD, DiceRoll.ELEVEN);
		addHex(3, 7, Resource.SHEEP, DiceRoll.TWELVE);
		addHex(5, 9, Resource.WHEAT, DiceRoll.NINE);
		
		addHex(1, 3, Resource.BRICK, DiceRoll.FOUR);
		addHex(3, 5, Resource.ROCK, DiceRoll.SIX);
		addHex(5, 7, Resource.BRICK, DiceRoll.FIVE);
		addHex(7, 9, Resource.SHEEP, DiceRoll.TEN);
		
		addHex(1, 1, Resource.DESERT, DiceRoll.SEVEN);
		addHex(3, 3, Resource.WOOD, DiceRoll.THREE);
		addHex(5, 5, Resource.WHEAT, DiceRoll.ELEVEN);
		addHex(7, 7, Resource.WOOD, DiceRoll.FOUR);
		addHex(9, 9, Resource.WHEAT, DiceRoll.EIGHT);

		addHex(3, 1, Resource.BRICK, DiceRoll.EIGHT);
		addHex(5, 3, Resource.SHEEP, DiceRoll.TEN);
		addHex(7, 5, Resource.SHEEP, DiceRoll.NINE);
		addHex(9, 7, Resource.ROCK, DiceRoll.THREE);
		
		addHex(5, 1, Resource.ROCK, DiceRoll.FIVE);
		addHex(7, 3, Resource.WHEAT, DiceRoll.TWO);
		addHex(9, 5, Resource.WOOD, DiceRoll.SIX);
	}
	
	public void createPorts() {
		edges.put(new Coordinate(0, 2), new Edge(0, 2));
		edges.put(new Coordinate(0, 5), new Edge(0, 5));
		edges.put(new Coordinate(3, 8), new Edge(3, 8));
		edges.put(new Coordinate(7, 10), new Edge(7, 10));
		edges.put(new Coordinate(10, 10), new Edge(10, 10));
		edges.put(new Coordinate(10, 7), new Edge(10, 7));
		edges.put(new Coordinate(8, 3), new Edge(8, 3));
		edges.put(new Coordinate(5, 0), new Edge(5, 0));
		edges.put(new Coordinate(2, 0), new Edge(2, 0));

	}	

	public Map<Coordinate, Hex> getHexes() {
		return hexes;
	}

	public void setHexes(Map<Coordinate, Hex> hexes) {
		this.hexes = hexes;
	}

	public Map<Coordinate, Point> getPoints() {
		return points;
	}

	public void setPoints(Map<Coordinate, Point> points) {
		this.points = points;
	}

	public Map<Coordinate, Edge> getEdges() {
		return edges;
	}

	public void setEdges(Map<Coordinate, Edge> edges) {
		this.edges = edges;
	}

	public void addHex(int x, int y, Resource resource, DiceRoll diceRoll) {
		Hex hex = new Hex(x, y, resource, diceRoll);
		generateHexValues(hex);	
	}
	
	public void generateHexValues(Hex hex) {	
		int x = hex.getCoordinate().getCoord1();
		int y = hex.getCoordinate().getCoord2();
		
		Edge currentEdge;
		// edges
		
		// // top left -1 0 
		currentEdge = getOrCreateEdge(x - 1, y);
		currentEdge.getHexes().add(hex);
		edges.put(new Coordinate(x - 1, y), currentEdge);
		hex.getEdges().add(currentEdge);
		
		
		// \\ top right 0 1
		currentEdge = getOrCreateEdge(x, y + 1);
		currentEdge.getHexes().add(hex);
		edges.put(new Coordinate(x, y + 1), currentEdge);
		hex.getEdges().add(currentEdge);
		
		// left side -1 -1
		currentEdge = getOrCreateEdge(x - 1, y - 1);
		currentEdge.getHexes().add(hex);
		edges.put(new Coordinate(x - 1, y - 1), currentEdge);
		hex.getEdges().add(currentEdge);
		
		// right side 1 1
		currentEdge = getOrCreateEdge(x + 1, y + 1);
		currentEdge.getHexes().add(hex);
		edges.put(new Coordinate(x + 1, y + 1), currentEdge);
		hex.getEdges().add(currentEdge);
		
		// bottom left \\ 0 -1
		currentEdge = getOrCreateEdge(x, y - 1);
		currentEdge.getHexes().add(hex);
		edges.put(new Coordinate(x, y - 1), currentEdge);
		hex.getEdges().add(currentEdge);
		
		// bottom right // 1 0
		currentEdge = getOrCreateEdge(x + 1, y);
		currentEdge.getHexes().add(hex);
		edges.put(new Coordinate(x + 1, y), currentEdge);
		hex.getEdges().add(currentEdge);
		
		// points
		Point currentPoint;

		// top 0 1
		currentPoint = getOrCreatePoint(x, y + 1);
		currentPoint.getHexes().add(hex);
		points.put(new Coordinate(x, y + 1), currentPoint);
		hex.getPoints().add(currentPoint);
		
		// upper left -1 0
		currentPoint = getOrCreatePoint(x - 1, y);
		currentPoint.getHexes().add(hex);
		points.put(new Coordinate(x - 1, y), currentPoint);
		hex.getPoints().add(currentPoint);
		
		// upper right 1 2
		currentPoint = getOrCreatePoint(x + 1, y + 2);
		currentPoint.getHexes().add(hex);
		points.put(new Coordinate(x + 1, y + 2), currentPoint);
		hex.getPoints().add(currentPoint);
		
		// lower left 0 -1
		currentPoint = getOrCreatePoint(x, y - 1);
		currentPoint.getHexes().add(hex);
		points.put(new Coordinate(x, y - 1), currentPoint);
		hex.getPoints().add(currentPoint);
		
		// lower right 2 1
		currentPoint = getOrCreatePoint(x + 2, y + 1);
		currentPoint.getHexes().add(hex);
		points.put(new Coordinate(x + 2, y + 1), currentPoint);
		hex.getPoints().add(currentPoint);
		
		// bottom 1 0
		currentPoint = getOrCreatePoint(x + 1, y);
		currentPoint.getHexes().add(hex);
		points.put(new Coordinate(x + 1, y), currentPoint);
		hex.getPoints().add(currentPoint);
		
		hexes.put(hex.getCoordinate(), hex);
		System.out.println("Hex put: " + hex.getCoordinate().getCoord1() + " : " + hex.getCoordinate().getCoord2());
		System.out.println("Edges:");
		for (Edge edge: hex.getEdges()) {
			System.out.println(edge.getCoordinate().getCoord1() + " : " + edge.getCoordinate().getCoord2());
		}
		System.out.println("");
		for (Point point: hex.getPoints()) {
			System.out.println(point.getCoordinate().getCoord1() + " : " + point.getCoordinate().getCoord2());
		}
	}
	
	Edge getOrCreateEdge(int x, int y) {
		Edge edge = edges.get(new Coordinate(x, y));
		return edge == null ? new Edge(x, y) : edge;
	}
	
	Point getOrCreatePoint(int x, int y) {
		Point point = points.get(new Coordinate(x, y));
		return point == null ? new Point(x, y) : point;
	}
	
	Point getPoint(Coordinate coordinate) {
		return points.get(coordinate);
	}
	
	Hex getHex(Coordinate coordinate) {
		return hexes.get(coordinate);
	}
	
	Hex getHex(int x, int y) {
		return hexes.get(new Coordinate(x, y));
	}
	
	Edge getEdge(Coordinate coordinate) {
		return edges.get(coordinate);
	}
	
	Edge getEdge(int x, int y) {
		return edges.get(new Coordinate(x, y));
	}
	
	public Boolean isSettleable(Coordinate coordinate) {
		Point potentialPoint = points.get(coordinate);
		
		if (potentialPoint.player == null) {
		
			for (Coordinate coord: potentialPoint.getAdjacentPoints()) {
				if (points.get(coordinate) != null && points.get(coordinate).getPlayer() != null) {
					return false;
				}
			}
			
			return true;
			
		}
		else {
			return false;
		}
					
	}
	
	public List<Coordinate> getSettleablePoints() {
		List<Coordinate> moves = new ArrayList<>();
		
		for (Coordinate point : points.keySet()) {
			if (isSettleable(point)) {
				moves.add(point);
			}
		}
		
		return moves;
	}
	
	public Map<Resource, Integer> getPlayerResources(Integer player) {
		Map<Resource, Integer> resourceMap = new HashMap<>();
		
		resourceMap.put(Resource.WOOD, 0);
		resourceMap.put(Resource.BRICK, 0);
		resourceMap.put(Resource.WHEAT, 0);
		resourceMap.put(Resource.SHEEP, 0);
		resourceMap.put(Resource.ROCK, 0);
		
		for (Entry<Coordinate, Point> point : points.entrySet()) {
		    Coordinate key = point.getKey();
		    Point value = point.getValue();
		    
		    if (value.getPlayer() == player) {
		    	for (Hex hex : value.getHexes()) {
		    		int resourceValue = hex.getDiceRoll().getOdds();
		    		if (value.getIsCity()) {
		    			resourceValue *= 2;
		    		}
		    		
		    		resourceMap.put(hex.getResource(), resourceMap.get(hex.getResource()) + resourceValue);
				}
		    }  

		}
		
		return resourceMap;
	}
	
	public List<Hex> getHexesForPoint(Coordinate coordinate) {
		List<Hex> adjacentHexes = new ArrayList<>();
		if (coordinate.getCoord1() % 2 == 0) {
			//u
			adjacentHexes.add(getHex(coordinate.getCoord1() - 1, coordinate.getCoord2()));
			//ll
			adjacentHexes.add(getHex(coordinate.getCoord1() - 1, coordinate.getCoord2() - 1));
			//lr
			adjacentHexes.add(getHex(coordinate.getCoord1() + 1, coordinate.getCoord2()));
		} 
		else {
			//ul
			adjacentHexes.add(getHex(coordinate.getCoord1() - 2, coordinate.getCoord2() - 1));
			//ur
			adjacentHexes.add(getHex(coordinate.getCoord1(), coordinate.getCoord2() + 1));
			//l
			adjacentHexes.add(getHex(coordinate.getCoord1(), coordinate.getCoord2() - 1));
		}
		return adjacentHexes;
	}
	
	public List<Edge> getEdgesForPoint(Coordinate coordinate) {
		List<Edge> adjacentEdges = new ArrayList<>();
		if (coordinate.getCoord1() % 2 == 0) {
			//ul
			adjacentEdges.add(getEdge(coordinate.getCoord1() - 1, coordinate.getCoord2() - 1));
			//ur
			adjacentEdges.add(getEdge(coordinate.getCoord1(), coordinate.getCoord2()));
			//l
			adjacentEdges.add(getEdge(coordinate.getCoord1(), coordinate.getCoord2() - 1));
		} 
		else {
			//u
			adjacentEdges.add(getEdge(coordinate.getCoord1() - 1, coordinate.getCoord2()));
			//ll
			adjacentEdges.add(getEdge(coordinate.getCoord1() - 1, coordinate.getCoord2() - 1));
			//lr
			adjacentEdges.add(getEdge(coordinate.getCoord1(), coordinate.getCoord2()));

		}
		return adjacentEdges;
	}
	
}
