package ai;

import java.util.ArrayList;
import java.util.List;

import ai.enums.Port;

public class Edge {
	Coordinate coordinate;
	
	private Port port;
	
	private List<Hex> hexes = new ArrayList<>();

	Edge(Coordinate coord) {
		this.coordinate = coord;
	}
	
	Edge(int x, int y) {
		this.coordinate = new Coordinate(x, y);
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Coordinate coord) {
		this.coordinate = coord;
	}

	public Port getPort() {
		return port;
	}

	public void setPort(Port port) {
		this.port = port;
	}

	public List<Hex> getHexes() {
		return hexes;
	}

	public void setHexes(List<Hex> hexes) {
		this.hexes = hexes;
	}
	
	
	
}
