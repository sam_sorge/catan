package ai;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Point {

	Coordinate coordinate;
	Integer player;
	Boolean isCity;
	
	List<Hex> hexes = new ArrayList<>();
	
	public Point(int x, int y) {
		this.coordinate = new Coordinate(x, y);
	}

	public Point(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

	public List<Hex> getHexes() {
		return hexes;
	}

	public void setHexes(List<Hex> hexes) {
		this.hexes = hexes;
	}

	public Integer getPlayer() {
		return player;
	}

	public void setPlayer(Integer player) {
		this.player = player;
	}

	public Boolean getIsCity() {
		return isCity;
	}

	public void setIsCity(Boolean isCity) {
		this.isCity = isCity;
	}

	public List<Coordinate> getAdjacentPoints() {
		List<Coordinate> coords = new ArrayList<Coordinate>();
//		id first coordinate is even
		if (coordinate.getCoord1() % 2 == 0) {
			//ur
			coords.add(new Coordinate(coordinate.getCoord1() - 1, coordinate.getCoord2() - 1));
			//ul
			coords.add(new Coordinate(coordinate.getCoord1() + 1, coordinate.getCoord2() + 1));
			//l
			coords.add(new Coordinate(coordinate.getCoord1() + 1, coordinate.getCoord2() - 1));
		} 
//		if first coordinate is odd
		else {
			//ll
			coords.add(new Coordinate(coordinate.getCoord1() - 1, coordinate.getCoord2() - 1));
			//lr
			coords.add(new Coordinate(coordinate.getCoord1() + 1, coordinate.getCoord2() + 1));
			//u
			coords.add(new Coordinate(coordinate.getCoord1() - 1, coordinate.getCoord2() + 1));
		}
		
		return coords;
	}
	

	
	
	
	
}
